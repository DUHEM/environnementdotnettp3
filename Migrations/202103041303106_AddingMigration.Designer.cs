﻿// <auto-generated />
namespace Tp3EnvironnementDotNet.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddingMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddingMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "202103041303106_AddingMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
