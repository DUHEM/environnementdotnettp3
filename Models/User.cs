﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TP3.Models
{
    public class User : EntityBase
    {
        [Column("firstname")]
        [Required(ErrorMessage = "Firstname is Required.")]
        [MaxLength(75, ErrorMessage = "Please enter less than 75 characters")]
        [MinLength(3, ErrorMessage = "Please enter more than 3 characters")]
        public string Firstname { get; set; }
        [Column("lastname")]
        [Required(ErrorMessage = "Lastname is Required.")]
        [MaxLength(75, ErrorMessage = "Please enter less than 75 characters")]
        [MinLength(3, ErrorMessage = "Please enter more than 3 characters")]
        public string Lastname { get; set; }
        [Column("role")]
        public virtual Role Role { get; set; }
        public virtual List<Book> Books { get; set; }
        public virtual List<Role> Roles { get; set; }

    }
}