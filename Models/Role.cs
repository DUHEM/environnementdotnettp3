﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TP3.Models
{
    public class Role : EntityBase
    {
        [Column("name")]
        [Required(ErrorMessage = "Name is Required.")]
        [MaxLength(75, ErrorMessage = "Please enter less than 75 characters")]
        [MinLength(3, ErrorMessage = "Please enter more than 3 characters")]
        public string Name { get; set; }
    }
}