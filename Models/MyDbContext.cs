﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TP3.Models;

namespace Tp3EnvironnementDotNet.Models
{
    public class MyDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public MyDbContext()
        {
            Configuration.LazyLoadingEnabled = true;

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasOptional(x => x.Role).WithMany();
            modelBuilder.Entity<User>().HasMany(x => x.Roles).WithMany();
        }

    }
}