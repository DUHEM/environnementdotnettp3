﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace Tp3EnvironnementDotNet.Controllers
{
    public class RoleController : Controller
    {
        private Models.MyDbContext DbContext;
        public RoleController()
        {
            DbContext = new Models.MyDbContext();
        }
        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        // GET: Role/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Role/Create
        public ActionResult RoleInfo()
        {
            return View();
        }

        // POST: Role/Create
        [HttpPost]
        public ActionResult RoleInfo(Role role)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("RoleInfo", role);
                }
                DbContext.Roles.Add(role);
                DbContext.SaveChanges();
                return RedirectToAction("RolesList");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult RolesList()
        {
            var roles = DbContext.Roles.ToList();
            return View(roles);
        }

        // GET: Role/Edit/5
        public ActionResult Edit(int id)
        {
            var role = DbContext.Roles.FirstOrDefault(e => e.Id == id);
            return View("RoleInfo", role);
        }

        // POST: Role/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Role role)
        {
            try
            {
                var dbRole = DbContext.Roles.Find(id);
                TryUpdateModel(dbRole, "", new string[] { "name" });
                DbContext.SaveChanges();
                return View("RoleInfo", dbRole);
            }
            catch
            {
                return View();
            }
        }

        // GET: Role/Delete/5
        public ActionResult Delete(int id)
        {
            var role = DbContext.Roles.FirstOrDefault(e => e.Id == id);
            foreach(User user in DbContext.Users.ToList())
            {
                if (user.Role.Id == id)
                {
                    DbContext.Users.Remove(user);
                }
            }
            DbContext.Roles.Remove(role);
            DbContext.SaveChanges();
            return RedirectToAction("RolesList");
        }

        // POST: Role/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var role = DbContext.Roles.FirstOrDefault(e => e.Id == id);
                DbContext.Roles.Remove(role);
                return RedirectToAction("RolesList");
            }
            catch
            {
                return View();
            }
        }
    }
}
