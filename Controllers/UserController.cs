﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace Tp3EnvironnementDotNet.Controllers
{
    public class UserController : Controller
    {
        private Models.MyDbContext DbContext;
        public UserController()
        {
            DbContext = new Models.MyDbContext();
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UsersList()
        {
            var users = DbContext.Users.ToList();
            return View(users);
        }

        // GET: User/UserInfo
        public ActionResult UserInfo()
        {
            return View(new User() { Roles = DbContext.Roles.ToList()});
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult UserInfo(User user,string role)
        {
            try
            {
                user.Roles = DbContext.Roles.ToList();
                if(role != null)
                {
                    foreach(Role dbRole in DbContext.Roles.ToList())
                    {
                        if (dbRole.Id.Equals(int.Parse(role)))
                        {
                            user.Role = dbRole;
                        }
                    }
                }
                //Bad process instead use !ModelState.isValid
                if (ModelState.Values.ElementAt(0).Errors.Count.Equals(1) || ModelState.Values.ElementAt(1).Errors.Count.Equals(1) || role == null)
                {
                    return View("UserInfo", user);
                }
                DbContext.Users.Add(user);
                DbContext.SaveChanges();
                return RedirectToAction("UsersList");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            var user = DbContext.Users.FirstOrDefault(e => e.Id == id);
            user.Roles = DbContext.Roles.ToList();
            return View("UserInfo", user);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, User user)
        {
            try
            {
                var dbUser = DbContext.Users.Find(id);
                user.Roles = DbContext.Roles.ToList();
                TryUpdateModel(dbUser,"",new string[] { "firstname", "lastname" });
                DbContext.SaveChanges();
                return View("UserInfo", dbUser);
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            var users = DbContext.Users.FirstOrDefault(e => e.Id == id);
            DbContext.Users.Remove(users);
            DbContext.SaveChanges();
            return RedirectToAction("UsersList");
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var user = DbContext.Users.FirstOrDefault(e => e.Id == id);
                DbContext.Users.Remove(user);
                return RedirectToAction("UsersList");
            }
            catch
            {
                return View();
            }
        }
    }
}
