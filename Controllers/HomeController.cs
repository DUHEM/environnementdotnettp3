﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tp3EnvironnementDotNet.Controllers
{
    public class HomeController : Controller
    {
        private Models.MyDbContext DbContext;
        public HomeController()
        {
            DbContext = new Models.MyDbContext();
        }
        public ActionResult Index()
        {
            var users = DbContext.Users.ToList();
            return View(users);
        }
    }
}