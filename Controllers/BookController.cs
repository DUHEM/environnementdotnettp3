﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP3.Models;

namespace Tp3EnvironnementDotNet.Controllers
{
    public class BookController : Controller
    {
        private Models.MyDbContext DbContext;
        public BookController()
        {
            DbContext = new Models.MyDbContext();
        }
        // GET: Book
        public ActionResult Index()
        {
            return View();
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Book/Create
        public ActionResult BookInfo()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult BookInfo(Book book)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View("BookInfo", book);
                }
                DbContext.Books.Add(book);
                DbContext.SaveChanges();
                return RedirectToAction("BooksList");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult BooksList()
        {
            var books = DbContext.Books.ToList();
            return View(books);
        }
        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            var book = DbContext.Books.FirstOrDefault(e => e.Id == id);
            return View("BookInfo", book);
        }

        // POST: Book/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var dbBook = DbContext.Books.Find(id);
                TryUpdateModel(dbBook, "", new string[] { "name", "nb_page","price" }); ;
                DbContext.SaveChanges();
                return View("BookInfo", dbBook);
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            var books = DbContext.Books.FirstOrDefault(e => e.Id == id);
            DbContext.Books.Remove(books);
            DbContext.SaveChanges();
            return RedirectToAction("BooksList");
        }

        // POST: Book/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var book = DbContext.Books.FirstOrDefault(e => e.Id == id);
                DbContext.Books.Remove(book);
                return RedirectToAction("BooksList");
            }
            catch
            {
                return View();
            }
        }
    }
}
